## Add sort button in to listing page by price

1. Modify *index.html.twig* file to similar to this.
```html
<div class="col-9">
    <div class="container">
        <div class="row">
            <div class="col-6"><h4>Product Listing</h4></div>
            <div class="col-3 text-end"><p>Price: </p></div>
            <div class="col-3">
                <a href="{{ path(app.request.attributes.get('_route'),
                    app.request.query.all|merge({'sort': 'Price', 'order': 'desc'})) }}"
                    class="btn btn-primary btn-sm">Hi->Low</a>
                <a href="{{ path(app.request.attributes.get('_route'),
                    app.request.query.all|merge({'sort': 'Price', 'order': 'asc'})) }}"
                    class="btn btn-primary btn-sm">Low->Hi</a>
            </div>
        </div>

        <div class="row row-cols-1 row-cols-md-4 g-4">
            {% for product in products %}
                <!-- same OLD code -->
            {% endfor %}
        </div>
    </div>
</div>
```

2. Modify *ProductController.php* to similar like this in **index** function

```php
public function index(ProductRepository $productRepository, Request $request): Response
{
    $selectedCategory = $request->query->get('category');
    $minPrice = $request->query->get('minPrice');
    $maxPrice = $request->query->get('maxPrice');
    $sortBy = $request->query->get('sort');
    $orderBy = $request->query->get('order');

    $expressionBuilder = Criteria::expr();
    $criteria = new Criteria();
    if (empty($minPrice)) {
        $minPrice = 0;
    }
    $criteria->where($expressionBuilder->gte('Price', $minPrice));
    if (!is_null($maxPrice) && !empty(($maxPrice))) {
        $criteria->andWhere($expressionBuilder->lte('Price', $maxPrice));
    }
    if (!is_null($selectedCategory)) {
        $criteria->andWhere($expressionBuilder->eq('Category', $selectedCategory));
    }
    if(!empty($sortBy)){
        $criteria->orderBy([$sortBy => ($orderBy == 'asc') ? Criteria::ASC : Criteria::DESC]);
    }
    $filteredList = $productRepository->matching($criteria);
    return $this->renderForm('product/index.html.twig', [
        'products' => $filteredList,
        'selectedCat' => $selectedCategory ?: 'Drink'
    ]);
}
```