
## Disable some fields (to be updated) in the form when editing record

We gonna make *Name* and *Category* become unable to update when doing **Edit** (not for **New** an object)

1. Make some changes in `ProductType.php`
```php
class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('Name', TextType::class, ['disabled' => $options['no_edit']])
            ->add('Category', TextType::class, ['disabled' => $options['no_edit']])
            ->add('Description')
            ->add('Price')
            ->add('weight');
    }


    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
            'no_edit' => false
        ]);
    }
}
```

2. Under `ProductController.php`, change to this
```php
public function edit(Request $request, Product $product, ProductRepository $productRepository): Response
    {
        $form = $this->createForm(ProductType::class, $product, array("no_edit" => true));
        //Same old code
    }
```
