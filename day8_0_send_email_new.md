# Configure email service and Send email with Gmail.

*Since the new package **symfony/mailer** are buggy, silly and working unstably*,\
 so we move to use the traditional **Swift Mailer** which is more stable and works on my computers and other friends for many email services including common personal Gmail.

So FORGET about the `day8_0_send_email.md` tutorial.

## I. Prepare Gmail setting to support SMTP.
1. Login and gain to 2-Step Verification (Add Phone Number to receive OTP maybe).
2. Go to this https://myaccount.google.com/apppasswords , type password again before going to **App Password** (<ins>this is special password used for only each one application like Email Client</ins>)
3. Then you gonna have a screen like below image, choose options as below and click `Generate`.
![App Password 1st](https://devanswers.co/wp-content/uploads/2017/02/apppw.png)
4. Next you gonna have a screen *similar* to below (<ins>similar</ins> means don't copy my example password below!)
![App Password 2nd](https://devanswers.co/wp-content/uploads/2017/02/create-app-password-16-digit-passcode-google.png)
5. Copy & Save the **App Password** in *Yellow* area above (16 characters <ins>without space</ins>), Press `Done`.


## II. Configure **Swift Mailer**
1. Install the bundle
`composer require symfony/swiftmailer-bundle`
2. Open `.env` file, remove all **symfony/mailer** stuff (i.e. remove all lines having `MAILER_DSN`)
3. Edit the file to look like this in `swiftmailer-bundle` section.

```yaml
###> symfony/swiftmailer-bundle ###
# MAILER_URL=gmail://username:app_password@host?encryption=tls&auth_mode=oauth
MAILER_URL=gmail://thaodo@gmail.com:anxnbmiolmnpfcci@smtp.gmail.com?encryption=tls&auth_mode=oauth
###< symfony/swiftmailer-bundle ###
```

## III. Send email using the configured service

1. Put this route where you want to put.

```php
/**
 * @Route("/sendmail", name="app_user_sendmail", methods={"GET"})
 */
public function sendmail(\Swift_Mailer $mailer): Response
{
    $message = (new \Swift_Message('Hello Email'))
        ->setFrom('thaodo@gmail.com')
        ->setTo('target_address@hotmail.com')
        ->setSubject("3rd Test send email")
        ->setBody("3rd Test send email");

    $mailer->send($message);
    return new Response("Send mail successfully");
}
```

2. Test to see if `target_address@hotmail.com` got the new email.