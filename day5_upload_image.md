# How to set up to upload file to image path in DB.

In this scenario, we gonna make the create/new product with ability to upload the product thumbnail to its.\
- So 1st, we change the DB/entity to have ImgUrl (image path) property.
- Then, we change to `new` form/template to have Image file submission field.
- After that, in Controller we save the Image file to a certain directory.
- We gonna get ProductId (primary key) + original filename's extention to use as *new Image filename* (to avoid overwrite other's file)
- Finally we use *new Image filename* to save in ImgUrl property in DB (for later showing out again)

## I. Steps
1. Add one property in varchar to be image path for thumbnails of products using `make:entity` to alter the table/entity. **Don't forget to sync changes to DB!**
2. In the `ProductType`, change it to similar like this
```php
<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id')
            ->add('ProductName')
            ->add('Price')
            ->add('Image', FileType::class, [
                'label' => 'Product Thumbnail',
                // unmapped means that this field is not associated to any entity property
                'mapped' => false,
                // every time you edit the Product details
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
```

3. In `services.yaml` try to put this var in param area like this 
```yaml
parameters:
    product_directory: '%kernel.project_dir%/public/images/product'
```

4. In `ProductController`, change the `new` function to similar to this:
```php
/**
     * @Route("/new", name="app_product_new", methods={"GET", "POST"})
     */
    public function new(Request $request, ProductRepository $productRepository): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $productImg = $form->get('Image')->getData();
            if ($productImg) {
                $originExt = pathinfo($productImg->getClientOriginalName(), PATHINFO_EXTENSION);
                $newName = $product->getId() . '.' . $originExt;

                try {
                    $productImg->move(
                        $this->getParameter('product_directory'),
                        $newName
                    );
                } catch (FileException $e) {
                }
                $product->setImgUrl($newName);
            }

            $productRepository->add($product, true);
            return $this->redirectToRoute('app_product_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('product/new.html.twig', [
            'product' => $product,
            'form' => $form,
        ]);
    }
```

## II. Caution!
For entities using **Auto Incremental (Identity) Primary Key**, you must run `$productRepository->add($product, true);` once before getId() since `Id` is not submitted from the form (Client side) but from the DB side.