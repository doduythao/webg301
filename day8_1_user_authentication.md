# Register, Login, Logout basic
Watch this [video](https://www.youtube.com/watch?v=AJZML9SYSTk). Or you can skip it.

## I. Preparation (new project, bootstrap, header, footer)
1. Follow instruction in `day6_bootstrap_header_footer` tutorial if you haven't done yet.
2. Create an entity like **Product** with `Name`, `Category` and `Price`; Scaffold `CRUD` **Product** to have index page for `/product/`.
3. Insert some data to **Product** table in DB.

## II. Main Steps
1. Install *security bundle*
```
composer require symfony/security-bundle
```

2. Scaffold `User` (this is **Security User** powered by Symfony, *not absolutely the same* as `User` you could make with make:entity)

<ins>Warning!</ins> **If you already had** `User` entity in the project, then name it to something else like `AppUser`, and **from now**, make sure it is `AppUser`, not `User`.

```
php bin/console make:user
The name of the security user class (e.g. User) [User]:
 > User

Do you want to store user data in the database (via Doctrine)? (yes/no) [yes]:
 > yes

Enter a property name that will be the unique "display" name for the user (e.g. email, username, uuid) [email]:
 > email

Does this app need to hash/check user passwords? (yes/no) [yes]:
 > yes
```

3. Migrate the *security user* `User` (or `AppUser`) into DB\
(make sure to setup `DATABASE_URL` in `.env` before)
```powershell
symfony console make:migration
symfony console doctrine:migrations:migrate
```

4. Make registration form (Register/Sign-up form)
```
symfony console make:registration-form

Creating a registration form for App\Entity\User

Do you want to add a @UniqueEntity validation ... ? (yes/no) [yes]:
 > yes

Do you want to send an email to verify ... ? [yes]:
 > no

Do you want to automatically authenticate ... ? [yes]:
 > yes

What route should the user be redirected to after registration ?:
 [0] blah blah blah
 [1] app_product_index (created product index before!)
 [2] blah blah blah
 [3] ...

 > 1
```

5. Fix registration-form template a bit by opening `register.html.twig` in `templates/registration` dir, change the *block body* to
```twig
{% block body %}
    <div class="container-md col-md-4 col-md-offset-4">
        <h1>Register</h1>
        {{ form_start(registrationForm, { 'attr' : { 'class': 'form-control' } }) }}
        {{ form_row(registrationForm.email) }}
        {{ form_row(registrationForm.plainPassword, {
            label: 'Password'
        }) }}
        {{ form_row(registrationForm.agreeTerms) }}
        <button type="submit" class="btn btn-primary btn-sm">Register</button>
        {{ form_end(registrationForm) }}
    </div>
{% endblock %}
```
<ins>From now</ins>, you could go to `localhost:8000/register` to register an account.\
 After pressing `Register`, (if no error) your account is created and the web is redirected to product index page.

6. Make log-in, log-out features
```
symfony console make:auth
What style of authen do you want ? [Empty authen]:
[0] Empty authen
[1] Login form authen
 > 1

The class name of the authen to create (e.g. ...):
 > LoginFormAuthenticator

Choose a name for the controller class (e.g. ...) [SecurityController]:
 > SecurityController

Do you want to generate a `/logout` URL ? (yes/no) [yes]:
 > yes
```

7. Fix `LoginFormAuthenticator.php` in `src/Security` dir:\
Get down and find `function onAuthenticationSuccess`
and replace *TODO* stuff by changing the whole function to
```php
public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
{
    if ($targetPath = $this->getTargetPath($request->getSession(), $firewallName)) {
        return new RedirectResponse($targetPath);
    }
    return new RedirectResponse($this->urlGenerator->generate('app_product_index'));
}
```

<ins>This</ins> `new RedirectResponse($this->urlGenerator->generate('app_product_index'));` means we redirect to product index page after we login successfully (with correct user/password of course)

8. Open `config/packages/security.yaml` file,\
 go to `logout:` in `main:` in `firewalls:` in `security:` \
 change this area to like this

```yaml
logout:
    path: app_logout
    # where to redirect after logout
    target: app_product_index
```

9. Change the navigation template (`_nav.html.twig`) a bit to have log-in, log-out link in navigation bar.\
Add *below code* **after** this line\
 `<li class="nav-item"><a href="#" class="nav-link">About</a></li>`
```twig
<li class="nav-item"><a href="{{ path('app_product_index') }}" class="nav-link">Products</a></li>
{% if app.user %}
    <li class="nav-item"><a class="nav-link">Hello {{ app.user.email }}</a></li>
{% endif %}
<li class="nav-item">
    <a href="/{{ app.user ? 'logout' : 'login' }}" class="nav-link">
        {{ app.user ? 'Logout' : 'Login' }}
    </a>
</li>
```

<ins>Notice</ins> `{% if app.user %}`, this is the code to check if you're logged-in or not in Twig.

10. Done. Now go to product index (`localhost:8000/product/`) in your browser and click Login, Logout to try.


