# New project with Boostrap-ready and header, footer.

## Download, Install NodeJS (to use `npm`)
0. Skip all if you already installed `npm`
1. Download from this [https://nodejs.org/en/download/](https://nodejs.org/en/download/)
2. Install as usual (just `next`)

## Create WebApp symfony project
0. Open Powershell (or your own command line) 
1. `cd` to somewhere you want to save the project.
2. Create a symfony project named *day8* with `symfony new --webapp day8`
3. `cd` into day8 directory.
4. Open the project by *PhpStorm* IDE
5. Open `.env` file in `/` and change `DATABASE_URL` to this
```yaml
DATABASE_URL="mysql://root@127.0.0.1:3306/day8?serverVersion=mariadb-10.4.24&charset=utf8mb4"
```

## Add Bootstrap CDN to the project
1. Open file `base.html.twig`, replace `{% block stylesheets %}` block by
```twig
{% block stylesheets %}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
            integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor"
            crossorigin="anonymous">
    {{ encore_entry_link_tags('app') }}
{% endblock %}
```

2. Similarly, replace `{% block javascripts %}` block by
```twig
{% block javascripts %}
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2"
            crossorigin="anonymous"></script>
    {{ encore_entry_script_tags('app') }}
{% endblock %}
```

## Install Bootstrap Module by `npm` to support auto-completion in PhpStorm IDE

`npm install bootstrap --save-dev`

After this installation, you gonna have auto-completion feature for bootstrap class while coding using PhpStorm (i.e. you don't have to remember all bootstrap class)

## Create header (navigation bar), footer template for the website

0. Expand `templates` dir in IDE
1. Create `_nav.html.twig` file on the same par with `base.html.twig`
2. Edit the `_nav.html.twig` file as below
```twig
<div class="container">
    <header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
        <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
            <span class="fs-4">Simple header</span>
        </a>

        <ul class="nav nav-pills">
            <li class="nav-item"><a href="#" class="nav-link active" aria-current="page">Home</a></li>
            <li class="nav-item"><a href="#" class="nav-link">About</a></li>
        </ul>
    </header>
</div>
```

3. Create `_footer.html.twig` file on the same par with `base.html.twig`, edit as below
```twig
<div class="container">
    <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
        <p class="col-md-4 mb-0 text-muted">© 2022 WebG301 - 0905, Sum</p>

        <a href="/"
           class="col-md-4 d-flex align-items-center justify-content-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
        </a>

        <ul class="nav col-md-4 justify-content-end">
            <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Home</a></li>
            <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">About</a></li>
        </ul>
    </footer>
</div>
```

4. Include the header and footer templates to `base.html.twig` by changing `<body></body>` block to this
```twig
<body>
{{ include('_nav.html.twig') }}
{% block body %}{% endblock %}
{{ include('_footer.html.twig') }}
</body>
```

5. Done. Try to create an entity like Product, create index showing all products and see the difference with Bootstrap.