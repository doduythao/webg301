# Install Symfony from scratch (full flow)
## Install XAMPP
### Steps:
0. Check if you had XAMPP version 7.x already ? If **yes**, skip installing XAMPP. Otherwise, just **remove** your XAMPP **completely**!
1. https://www.apachefriends.org/download.html
2. Pick version 7.x (*Don't pick version 8*) for Windows or up to you.
3. Install it, then Open it with Admin right.
4. Click `Start` for only `Apache` & `MySQL`.
5. Check if `Apache` & `MySQL` run successfully (green tick) with PID and Port.

### Trouble shooting (in case you got it)
- if `5.` step has problem, try to change port. Or ask lecturer!

## Install Composer
### Steps:

1. Download from here https://getcomposer.org/download/
2. (Windows) Install by run the downloaded file (`Composer-Setup.exe`)
3. Check if Composer installed successfully by `composer -V` in Powershell

## Install Scoop
1. Turn on PowerShell.
2. Run this:

```powershell
Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
Invoke-WebRequest get.scoop.sh -UseBasicParsing | Invoke-Expression
```
3. Check if Scoop installed successfully by `scoop -V` in Powershell.

## Install Symfony

1. Run this `scoop install symfony-cli` in Powershell
2. After symfony installation finished, try to create the first webapp project by `symfony new --webapp my_project`
3. Change directory to the `my_project` folder.
```
cd my-project/
```
4. Init running the symfony `my_project` project to observe.
```
symfony server:start
```