## How to get URL (get) params in Twig

You can use `app.request.query.all` to get your query strings.

1. If you want to change a param in Twig you can do this
```php
{% set queryParams = app.request.query.all %}
{% set queryParams = queryParams|merge({queryKey: newQueryValue}) %}
```

2. To get the query string "https://stackoverflow.com?name=jazz"
```php
{{ app.request.query.get('name') | default('default value if not set'); }}
```