# Set up mail service to send email 

(Use cases like send *Registration confirm email* or *send an email after customer checkout an order*)

## I. Install dependency 
Run `composer require symfony/mailer` at your project directory

## II. Prepare SMTP configuration account by your Gmail account

  1. Login your gmail, turn off 2-step-verification (if available).
  2. Go to this: https://myaccount.google.com/lesssecureapps
  3. Turn the option **On** (blue/green color) not grey
  4. Remember this information to fill later.
```
Gmail SMTP server address: smtp.gmail.com
Gmail SMTP name: <Your google shown name>
Gmail SMTP username: <Your full Gmail address> (e.g. your@gmail.com)
Gmail SMTP password: <The password you use to login> (e.g. qwerty123)
Gmail SMTP port (TLS): 587
```

## III. Set up email configuration info in `.env` file
  1. Go to `.env` file, open it
  2. Find this similar snippet and change to **your information** (what you got from *II.4*)

```yaml
###> symfony/mailer ###
###> MAILER_DSN=smtp://user:pass@smtp.example.com:port ###
MAILER_DSN=smtp://your@gmail.com:qwerty123@smtp.gmail.com:587
###< symfony/mailer ###
```

## IV. Use the email service to send email.
  1. Go to a controller you want to use (e.g. `UserController.php`)
  2. Add new function *similar* to this (change to **your own info**)
```php
/**
 * @Route("/sendmail", name="app_user_sendmail", methods={"GET"})
 */
public function sendmail(MailerInterface $mailer): Response
{
    $email = (new Email())
        ->from('your@gmail.com')
        ->to('<your-target-address>@gmail.com')
        ->subject('Time for Symfony Mailer!')
        ->text('Sending emails is fun again!')
        ->html('<p>See Twig integration for better HTML integration!</p>');

    try {
        $mailer->send($email);
    } catch (TransportExceptionInterface $e) {
    }
    return new Response();
}
```

  3. Import required libraries (add to import list)
```php
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
```
  
  4. Run the server, access to this url `http://localhost:8000/user/sendmail`
  5. Check if `<your-target-address>@gmail.com` receive the email?


## V. Regain security of your account after the step II.

*The step II. lowered security level of your gmail account. So after this experiment*

> Go to https://myaccount.google.com/lesssecureapps and turn it off.