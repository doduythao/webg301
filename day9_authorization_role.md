# Access Control (Authorization)

## First Experiment of Securing a Dashboard for a Role
1. Change your `app_product_index` function to similar to below code

```php
/**
 * @Route("/", name="app_product_index", methods={"GET"})
 */
public function index(ProductRepository $productRepository): Response
{
//        $this->denyAccessUnlessGranted('ROLE_USER');
    $hasAccess = $this->isGranted('ROLE_USER');
    if ($hasAccess) {
        return $this->render('product/index.html.twig', [
            'products' => $productRepository->findAll(),
        ]);
    } else {
        return $this->render('product/index.html.twig', [
            'products' => [],
        ]);
    }
}
```

2. Try to Logout and go to http://localhost:8000/product/ \
Observe what happen to all records ???

3. Try to Login and go to http://localhost:8000/product/ \
Observe what happen ?

4. Try to make this comment `// $this->denyAccessUnlessGranted('ROLE_USER');` to be effective again, then try http://localhost:8000/product/ again.

5. Open `Entity/User.php` (or `AppUser` if follows day8 tutorials),\
get down to `public function getRoles(): array` and see.

## User Hierachy

```yaml
# config/packages/security.yaml
security:
    # ...

    role_hierarchy:
        ROLE_ADMIN:       ROLE_USER
        ROLE_SUPER_ADMIN: [ROLE_ADMIN, ROLE_ALLOWED_TO_SWITCH]
```

## Add/Set/Promote a User to a Role.

- Open DB, go to table User, update `Roles` columns to `["ROLE_ADMIN"]` for example (other role up to you)

- Another way by code

```php
/**
 * @Route("/setRole", name="app_set_role", methods={"GET"})
 */
public function setRole(UserRepository $userRepository): JsonResponse
{
    /** @var \App\Entity\User $user */
    $user = $this->getUser();
    $user->setRoles(array('ROLE_ADMIN'));
    $userRepository->add($user, true);
    return $this->json(['username' => $this->getUser()->getUserIdentifier()]);
}
```