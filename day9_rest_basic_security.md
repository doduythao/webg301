# REST API 
![Rest API](https://ws.apms.io/api/_files/NydJSQz2pxfUmD5yTEe2FR/download/)

Normally people just use **GET/POST** to do all CRUD instead of using all methods *(PUT/DELETES/PATCH/HEAD/OPTIONS)*

Simple visualization of REST HTTP methods.
![get-post-delete](https://s3-us-west-2.amazonaws.com/assertible/blog/swagger-petstore-endpoint-methods.png)

## Anything compete with REST API ? SOAP, RPC....

Read [REST vs. SOAP](https://www.redhat.com/en/topics/integration/whats-the-difference-between-soap-rest)

![API architectural styles](https://www.altexsoft.com/media/2020/05/word-image-53.png)

## How to try HTTP methods

Some people tell you to try **Postman**. BUT since it is getting heavy and pretty big to download. I suggest to use *Thunder Client* [extension](https://marketplace.visualstudio.com/items?itemName=rangav.vscode-thunder-client) of VS Code. 

1. First, you need to disable CSRF protection for a form submission before trying to use POST/GET, like this form.
```php
$form = $this->createForm(ProductType::class, $product, array('csrf_protection' => false));
```
2. Then use the Tab *Thunder Client* and fill all the info for each request like POST or GET.\
- If POST, then form should be filled in `Body/Form`
- If GET, then form should be filled in `Query`.

# Basic Security

## CSRF attacks.

![CSRF attacks 1](https://qph.cf2.quoracdn.net/main-qimg-128d7b63046fc10e74bf6346e7da0ed6)

![CSRF attacks 2](https://reflectoring.io/images/posts/csrf/csrf-intro_hua1478ce17fadc128f4bcdf5651ad3b7f_65290_731x0_resize_box_3.png)

*<ins>Actually</ins> you don't need to disable CSRF protection in form setting.*\
CSRF protection uses: session cookies AND CSRF token at the same time to validate the genuine of the form submission (*Was it from the server really? or just made-up by hackers*)

So, from Thunder Client:
- First, just use `GET` to submit the form and get the response && the cookies.
- Then copy token from the response to make up the form body, submit the form again with `POST` and see, it is **accepted** now! (notice the change of cookie too).

## SQL Injection.

This topic is highly advanced. I just recommend to secure your web by using criteria for filtering, or using specific ORM in *<ins>EntityName</ins>Repository.php*.\
Avoid directly expose SQL [Query Builder](https://www.doctrine-project.org/projects/doctrine-orm/en/current/reference/query-builder.html)