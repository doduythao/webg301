## Pagination for product listing

Gonna have thing like this, each page has 8 products. Don't forget to read the comments in the code.

![extended image](img/day7_3.png)

0. **[2022-10-12 Update]** Change the function **index** in *ProductController* to look like this:
```php
/**
     * @Route("/{page<\d+>}", name="app_product_index", methods={"GET"})
     */
    public function index(Request           $request,
                          ProductRepository $productRepository,
                          int               $page = 1): Response
    {
        $minPrice = $request->query->get('minPrice');
        $maxPrice = $request->query->get('maxPrice');
        $cat = $request->query->get('category');
        if (!(is_null($cat) || empty($cat))) {
            $selectedCat = $cat;
        } else
            $selectedCat = '';
        $tempQuery = $productRepository->findMore($minPrice, $maxPrice, $cat);
        $pageSize = 4;

        // Load doctrine Paginator
        $paginator = new Paginator($tempQuery);

        // get total filtered items
        $totalItems = count($paginator);

        // get total pages
        $numOfPages = ceil($totalItems / $pageSize);

        // now get $page 's items:
        $products = $paginator
            ->getQuery()
            ->setFirstResult($pageSize * ($page - 1)) // set the offset
            ->setMaxResults($pageSize) // set the limit
            ->getResult();

        return $this->render('product/index.html.twig', [
            'products' => $products,
            'selectedCat' => $selectedCat,
            'numOfPages' => $numOfPages
        ]);
    }
```

and this in `ProductRepository` (Model):
```php
public function findMore($minPrice, $maxPrice, $Cat): Query
    {
        $entityManager = $this->getEntityManager();
        $qb = $entityManager->createQueryBuilder();
        $qb->select('p')
            ->from('App:Product', 'p');
        if (is_null($minPrice) || empty($minPrice)) {
            $minPrice = 0;
        }
        $qb->where('p.Price >=' . $minPrice);
        if (!(is_null($maxPrice) || empty($maxPrice))) {
            $qb->andWhere('p.Price <=' . $maxPrice);
        }
        if (!(is_null($Cat) || empty($Cat))) {
            $qb->andWhere('p.Category =' . $Cat);
        }
//        $qb->andWhere('p.Name LIKE :word')->setParameter('word', '%'.$word.'%');
        return $qb->getQuery();
    }
```

1. **(OLD solution, skip this)** Change the function **index** in *ProductController* to look like this:

```php
/**
 * @Route("/{pageId}", name="app_product_index", methods={"GET"})
 */
public function index(ProductRepository $productRepository, Request $request, int $pageId = 1): Response
{
    $selectedCategory = $request->query->get('category');
    $minPrice = $request->query->get('minPrice');
    $maxPrice = $request->query->get('maxPrice');
    //Same OLD code
    $filteredList = $productRepository->matching($criteria);

    $numOfItems = $filteredList->count();   // total number of items satisfied above query
    $itemsPerPage = 8; // number of items shown each page
    $filteredList = $filteredList->slice($itemsPerPage * ($pageId - 1), $itemsPerPage);
    return $this->renderForm('product/index.html.twig', [
        'products' => $filteredList,
        'selectedCat' => $selectedCategory ?: 'Drink',
        'numOfPages' => ceil($numOfItems / $itemsPerPage)
    ]);
}
```

2. Change the *index.html.twig* to look like this

```html
{% extends 'base.html.twig' %}

{% block title %}Product index{% endblock %}

{% block body %}
    <div class="container-md">
        <div class="row">
            <div class="col-3">
                <form action={{ path('app_product_index') }} method="get">
                    <!-- Same OLD code for sidebar-->
                </form>
            </div>
            <div class="col-9">
                <div class="container">
                    <div class="row">
                        <div class="col-6"><h4>Product Listing</h4></div>
                        <div class="col-3 text-end"><p>Price: </p></div>
                        <!-- Same OLD code for sorting price -->
                    </div>

                    <div class="row row-cols-1 row-cols-md-4 g-4">
                        {% for product in products %}
                            <div class="col">
                                <div class="card h-100">
                                    <!-- Same OLD code for product card -->
                                </div>
                            </div>
                        {% endfor %}
                    </div>
                    <div class="row mt-2">
                        <div style="display:flex;text-align:center;justify-content:center">
                            <nav aria-label="Page navigation">
                                <ul class="pagination">
                                    {% for i in range(1, numOfPages) %}
                                        {% set style = app.request.get('page')==i ? "active" : "" %}
                                        <li class="page-item {{ style }}">
                                            <a class="page-link"
                                               href={{ path(app.request.attributes.get('_route'),
                                                app.request.query.all|merge({'page': i})) }}>{{ i }}</a>
                                        </li>
                                    {% endfor %}
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}

```