## Create simple listing page for Customers (not for poster)
0. After create entity, run migration and import some data into it.
```sql
INSERT INTO `product` (`id`, `name`, `category`, `description`, `price`, `weight`) VALUES
(1, 'Coca', 'Drink', 'Coca Cola', 10, 300),
(2, 'TeaPlus', 'Drink', '', 15, 500),
(3, 'Yogurt', 'Drink', '', 12, 50),
(4, 'Potato Chip', 'Snack', '', 5, 100),
(5, 'Taro Chip', 'Snack', '', 6, 100),
(6, 'Corn Chip', 'Snack', '', 7, 100),
(7, 'Laptop ThinkPad T14', 'Electronics', '', 10000, 5000),
(8, 'Laptop ThinkPad E14', 'Electronics', '', 12000, 5000),
(9, 'Laptop ThinkPad P14', 'Electronics', '', 14000, 5000),
(10, 'iPhone 14', 'Electronics', '', 24000, 50);
```
1. In the controller, we need to modify like this, to handle parameters

```php
    /**
     * @Route("/", name="app_product_index", methods={"GET"})
     */
    public function index(ProductRepository $productRepository, Request $request): Response
    {
        $selectedCategory = $request->query->get('category');
        $minPrice = $request->query->get('minPrice');
        $maxPrice = $request->query->get('maxPrice');

        $expressionBuilder = Criteria::expr();
        $criteria = new Criteria();
        if (is_null($minPrice) || empty($minPrice)) {
            $minPrice = 0;
        }
        $criteria->where($expressionBuilder->gte('Price', $minPrice));
        if (!is_null($maxPrice) && !empty(($maxPrice))) {
            $criteria->andWhere($expressionBuilder->lte('Price', $maxPrice));
        }
        if (!is_null($selectedCategory)) {
            $criteria->andWhere($expressionBuilder->eq('Category', $selectedCategory));
        }
        $filteredList = $productRepository->matching($criteria);
        return $this->renderForm('product/index.html.twig', [
            'products' => $filteredList,
            'selectedCat' => $selectedCategory ?: 'Drink'
        ]);
    }
```

2. For the front-end `index.html.twig`
```html
{% extends 'base.html.twig' %}

{% block title %}Product index{% endblock %}

{% block body %}
    <div class="container-md">
        <div class="row">
            <div class="col-3">
                <form action={{ path('app_product_index') }} method="get">
                    Category:
                    <select name="category">
                        <option value="Drink" {{ (selectedCat=="Drink") ? 'selected' }}> Drink</option>
                        <option value="Snack" {{ (selectedCat=="Snack") ? 'selected' }}> Snack</option>
                        <option value="Electronics" {{ (selectedCat=="Electronics") ? 'selected' }} >
                            Electronics
                        </option>
                    </select><br>

                    Min Price: <input type="number" name="minPrice"><br>
                    Max Price: <input type="number" name="maxPrice"><br>
                    <input type="submit" value="Filter" class="btn btn-primary"> |
                    <a href="{{ path('app_product_index') }}"
                       class="btn btn-primary">Reset</a>
                </form>
            </div>
            <div class="col-9">
                <div class="container">
                    <h4>Product Listing</h4>
                    <div class="row row-cols-1 row-cols-md-4 g-4">
                        {% for product in products %}
                            <div class="col">
                                <div class="card h-100">
                                    <img class="card-img-top" src="{{ asset('images/logo.jpg') }}">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ product.Name }}</h5>
                                        <h6 class="card-subtitle">Category: {{ product.category }}</h6>
                                        <p>Price: {{ product.price }}</p>
                                    </div>
                                    <div class="card-footer">
                                        <a href="{{ path('app_product_show', {'id': product.id}) }}"
                                           class="btn btn-primary">Show</a>
                                    </div>
                                </div>
                            </div>
                        {% endfor %}
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}
```
