# How to Debug.

## Debug using Logger.
To log a message, inject the default logger in your controller or service:
```php
use Psr\Log\LoggerInterface;

public function index(LoggerInterface $logger)
{
    $logger->info('I just got the logger');
    $logger->error('An error occurred');

    /** @var \App\Entity\User $user */
    $user = $this->getUser();
    if (is_null($user))
        $logger->info("User is not logged in");
    else
        $logger->info("User's email is ".$user->getUserIdentifier());
}
```

## Debug using XDebug
TODO: TBD