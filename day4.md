## Day 4
### What to teach:
- Keep explaining on what scaffolding for only 1 single table.
- Changing the primary key on our own setting (we use own PK, not Integer, not auto increasing)
- Add **Asset** to the project like [this](https://symfony.com/doc/current/templates.html#linking-to-css-javascript-and-image-assets) to host images, files to server for the project.
- Reusing Template Contents by using (the _ prefix is optional, but it's a convention used to better differentiate between full templates and template fragments)
```php
{{ include('blog/_user_profile.html.twig') }}
```

- Append to new content to the code block instead of overriding (Twig)
```php
{% block css %}
    {{ parent() }}
    div a { color: #777; }
{% endblock css %}
```

- Redirecting (check this [link](https://symfony.com/doc/current/controller.html#redirecting))

- Redirect to Error or 404 pages (learn more [here](https://symfony.com/doc/current/controller.html#managing-errors-and-404-pages)), 500 error. (Still here!)

- Return a JSON to client side [check this](https://symfony.com/doc/current/controller.html#returning-json-response).

- Return a file to client (such as download a file feature), [check](https://symfony.com/doc/current/controller.html#streaming-file-responses).

```php
/**
 * @Route("/download", name="app_student_download", methods={"GET"})
 */
public function download(): BinaryFileResponse
{
    $projectDir = $this->getParameter('kernel.project_dir').'/public/';
    return $this->file($projectDir.'/binaries/xh.pdf');
}
```
