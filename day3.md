# How to make Basic CRUD

0. Create a database with <something> name. (e.g. **day1**)
1. Change connection string in `.env` file to match with your MariaDB in XAMPP.
```yaml
DATABASE_URL="mysqli://root:@127.0.0.1:3306/day1?serverVersion=mariadb-10.4.24"
```
2. Use this console `php .\bin\console make:entity` to run a procedure for making an entity (a table in DB). You will need to fill the name, attributes (properties) for this entity.
3. Run this `php bin/console make:migration` to create *migration plan*.\
This *migration plan* is how PHP gonna create SQL DDL to make DB Table so that it's gonna match with entity (in class).\
**Note that you should review it before synchronizing all changes to DB.**

4. Run `php bin/console doctrine:migrations:migrate` to finally commit your changes to DB.
5. Run `php .\bin\console make:crud` so Symfony console gonna help you scaffold some views and a controller for that Entity.